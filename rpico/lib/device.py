import machine
import time

led = machine.Pin("LED", machine.Pin.OUT)

def TurnOffLedIntent():
    led.off()
    
def TurnOnLedIntent():
    led.on()

def FlashLedIntent():
    length = 10
    period = 0.1	# seconds
    
    for i in range(length):
        led.on()
        time.sleep(period / 2.0)
        led.off()
        time.sleep(period / 2.0)
