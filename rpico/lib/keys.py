WIFI_SSID = "Your_Wifi_Name"
WIFI_PASSWORD = "Your_Wifi_Password"

MQTT_CLIENT_KEY = "Your_AWS_IoT_Key_Filename-private.pem.key"
MQTT_CLIENT_CERT = "Your_AWS_IoT_Cert_Filename-certificate.pem.crt"
MQTT_BROKER = "Your_AWS_IoT_Endpoint_Address.amazonaws.com"
MQTT_BROKER_CA = "AmazonRootCA1.pem"
MQTT_TOPIC = "raspberry_demo_topic"
